import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { PostsService } from 'src/app/core/services/posts.service';
import { Post } from 'src/app/models/post.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  security: string = 'A@123456';
  post: Post[];

  constructor(
    public authService: AuthService,
    public afAuth: AngularFireAuth,
    private postService: PostsService,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.postService.getPosts().subscribe(result => {
      this.post = result
      console.log(result)
    })
  }
}
