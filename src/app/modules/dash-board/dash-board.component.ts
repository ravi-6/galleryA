import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { LikesService } from 'src/app/core/services/likes.service';
import { PostsService } from 'src/app/core/services/posts.service';
import { UserService } from 'src/app/core/services/user.service';
import { Dislike } from 'src/app/models/dislike.model';
import { Like } from 'src/app/models/like.model';
import { Post } from 'src/app/models/post.model';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-dash-board',
  templateUrl: './dash-board.component.html',
  styleUrls: ['./dash-board.component.scss']
})
export class DashBoardComponent implements OnInit {

  numberOfLike: number = 0;
  numberOfDislike: number = 0;

  po: Post = new Post()
  isLiked: boolean = false;
  isDisliked: boolean = false;
  user: User;
  post: Post[];
  selectedPost: Post;
  likes: Like[];
  poste: Post;
  userId: string;
  USER_EMAIL: string;
  postID: string;
  postReference: AngularFirestoreDocument;
  like: any = [];
  like1: any = [];
  like2: any = [];
  dislike: any = [];
  dislike1: any = [];
  dislike2: any = [];
  likeLength: number;
  like1Length: number;
  like2Length: number;
  dislikeLength: number;
  dislike1Length: number;
  dislike2Length: number;
  uId: string;
  heartType: string = "heart-empty"
  id
  postId: string;
  post1Id: string;
  post2Id: string;

  constructor(
    public authService: AuthService,
    private likesService: LikesService,
    private userService: UserService,
    private postService: PostsService,
    public afAuth: AngularFireAuth,
    private route: ActivatedRoute,
    private afs: AngularFirestore,
  ) { }

  ngOnInit(): void {

    this.afAuth.authState.subscribe(user => {
      this.USER_EMAIL = user.email;
      this.uId = user.uid;
      console.log(this.uId)
      this.postService.getPosts().subscribe(result => {
        this.post = result
        this.postId = result[0].id;
        this.post1Id = result[1].id;
        this.post2Id = result[2].id;
        this.getLike()
        this.getLike1()
        this.getLike2()
        this.getDislike()
        this.getDislike1()
        this.getDislike2()
        this.getLikee()
        this.getLikee1()
        this.getLikee2()
        this.getDislikee()
        this.getDislikee1()
        this.getDislikee2()
      })
    })
  }

  getLikee() {
    this.likesService.getLike(this.postId, this.uId).subscribe(result => {
      this.likeLength = result.length
    })
  }

  getLikee1() {
    this.likesService.getLike(this.post1Id, this.uId).subscribe(result => {
      this.like1Length = result.length
    })
  }

  getLikee2() {
    this.likesService.getLike(this.post2Id, this.uId).subscribe(result => {
      this.like2Length = result.length
    })
  }

  getDislikee() {
    this.likesService.getDislike(this.postId, this.uId).subscribe(result => {
      this.dislikeLength = result.length
    })
  }

  getDislikee1() {
    this.likesService.getDislike(this.post1Id, this.uId).subscribe(result => {
      this.dislike1Length = result.length
    })
  }

  getDislikee2() {
    this.likesService.getDislike(this.post2Id, this.uId).subscribe(result => {
      this.dislike2Length = result.length
    })
  }

  getLike() {
    this.likesService.getLikeeee(this.postId, this.uId).subscribe(result => {
      this.like = result
      this.likeLength = result.length
      console.log(result)
    })
  }

  getDislike() {
    this.likesService.getDisikeeee(this.postId, this.uId).subscribe(result => {
      this.dislike = result
      console.log(result)
    })
  }

  getLike1() {
    this.likesService.getLikeeee1(this.post1Id, this.uId).subscribe(result => {
      this.like1 = result
      console.log(result)
    })
  }

  getDislike1() {
    this.likesService.getDisikeeee1(this.post1Id, this.uId).subscribe(result => {
      this.dislike1 = result
      console.log(result)
    })
  }

  getLike2() {
    this.likesService.getLikeeee(this.post2Id, this.uId).subscribe(result => {
      this.like2 = result
      console.log(result)
    })
  }

  getDislike2() {
    this.likesService.getDisikeeee(this.post2Id, this.uId).subscribe(result => {
      this.dislike2 = result
      console.log(result)
    })
  }

  likessss() {
    console.log("Like1", this.like1)
    if (this.like.length == 0) {
      this.numberOfDislike++;
      const addLike = this.likesService.addLikeeee(this.postId, this.uId);
      console.log("AddLike", addLike)
    } else {
      this.numberOfLike--;
      this.likesService.updateLikeeee(this.postId, this.like[0].id);
    }
  }

  dislikessss() {
    console.log("Like1", this.like1)
    if (this.dislike.length == 0) {
      this.numberOfDislike++;
      const addDislike = this.likesService.addDislikeeee(this.postId, this.uId, this.like[0].id);
      console.log("AddDislike", addDislike)
    } else {
      this.numberOfLike--;
      this.likesService.updateDislikeeee(this.postId, this.dislike[0].id);
    }
  }

  likessss1() {
    console.log("Like1", this.like1)
    if (this.like1.length == 0) {
      this.numberOfDislike++;
      const addLike = this.likesService.addLikeeee(this.post1Id, this.uId);
      console.log("AddLike", addLike)
    } else {
      this.numberOfLike--;
      this.likesService.updateLikeeee(this.post1Id, this.like1[0].id);
    }
  }

  dislikessss1() {
    console.log("Like1", this.like1)
    if (this.dislike1.length == 0) {
      this.numberOfDislike++;
      const addDislike = this.likesService.addDislikeeee(this.post1Id, this.uId, this.like1[0].id);
      console.log("AddDislike", addDislike)
    } else {
      this.numberOfLike--;
      this.likesService.updateDislikeeee(this.post1Id, this.dislike1[0].id);
    }
  }

  likessss2() {
    console.log("Like1", this.like1)
    if (this.like2.length == 0) {
      this.numberOfDislike++;
      const addLike = this.likesService.addLikeeee(this.post2Id, this.uId);
      console.log("AddLike", addLike)
    } else {
      this.numberOfLike--;
      this.likesService.updateLikeeee(this.post2Id, this.like2[0].id);
    }
  }

  dislikessss2() {
    console.log("Like1", this.like1)
    if (this.dislike2.length == 0) {
      this.numberOfDislike++;
      const addDislike = this.likesService.addDislikeeee(this.post2Id, this.uId, this.like2[0].id);
      console.log("AddDislike", addDislike)
    } else {
      this.numberOfLike--;
      this.likesService.updateDislikeeee(this.post2Id, this.dislike2[0].id);
    }
  }

  getUser() {
    this.userService.getUsers(this.USER_EMAIL).subscribe(user => {
      this.userId = user[0].id;
      // console.log(user)
    })
  }
}
