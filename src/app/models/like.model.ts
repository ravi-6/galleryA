export class Like {
    id: string;
    isLiked: boolean;
    updated_on: number;
    created_on: number;
    userId: string;
}
