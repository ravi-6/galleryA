export class User {
    id: string;
    uid: string;
    name: string;
    email: string;
    avatar: string;
    bio: string;
    gender: string;
    numberOfPosts: number;
    avatarStoragePath: string;
    created_on: number;
    like: string[];
}
