export class Post {
    id?: string;
    author: string;
    img: string;
    isLiked: boolean;
    isDisliked: boolean;
    deleted: boolean;
    createdOn: number;
    Likes: string[]; // emailIds
    Dislikes: string[];
}
