export class Dislike {
    id: string;
    isDisliked: boolean;
    updated_on: number;
    created_on: number;
    userId: string;
}
