import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private angularFirestore: AngularFirestore
  ) { }

  getUsers(USER_EMAIL) {
    const user = this.angularFirestore.collection('users', ref =>
      ref.where("emailId", "==", USER_EMAIL)).snapshotChanges().pipe(
        map(result => {
          return result.map(doc => {
            return {
              id: doc.payload.doc.id,
              ...doc.payload.doc.data() as User
            } as User;
          });
        })
      );
    return user;
  }


  // getUser(USER_EMAIL) {
  //   const user = this.angularFirestore.collection('users', ref =>
  //     ref.where("uid", "==", USER_EMAIL)).snapshotChanges().pipe(
  //       map(result => {
  //         return result.map(doc => {
  //           return {
  //             id: doc.payload.doc.id,
  //             ...doc.payload.doc.data() as User
  //           } as User;
  //         });
  //       })
  //     );
  //   return user;
  // }
}
