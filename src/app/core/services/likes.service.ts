import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Dislike } from 'src/app/models/dislike.model';
import { Like } from 'src/app/models/like.model';
import { Post } from 'src/app/models/post.model';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LikesService {
  postId: 'E5zoGQjZeSxws0iaFV37'
  postIdd: 'aqAPWjXEp5k3PUoqPkgF'
  postID: 'z0WXrfNLNJodwtUf5GKm'
  constructor(
    private angularFireStore: AngularFirestore
  ) { }
  getInsertLikeData(postId: string, likeData: Like) {
    this.angularFireStore.collection('posts').doc(postId).collection('likes').add(Object.assign({}, likeData));
  }

  async addLike(postId: string, likeData: Like, post: Post) {
    const LikeDocRef = this.angularFireStore.collection(`${"posts"}/${postId}/${"likes"}`).ref.doc();
    const postDocRef = this.angularFireStore.doc(`posts/${postId}`).ref;
    const batch = this.angularFireStore.firestore.batch();
    batch.set(LikeDocRef, { ...likeData });
    batch.update(postDocRef, { ...post });
    return batch.commit()
  }

  async addD(postId: string, likeData: Dislike, post: Post) {
    const DislikeDocRef = this.angularFireStore.collection(`${"posts"}/${postId}/${"dislikes"}`).ref.doc();
    const likeDocRef = this.angularFireStore.collection(`${"posts"}/${postId}/${"likes"}`).ref.doc();
    const postDocRef = this.angularFireStore.doc(`posts/${postId}`).ref;
    const batch = this.angularFireStore.firestore.batch();
    batch.set(DislikeDocRef, { ...likeData });
    batch.delete(likeDocRef);
    batch.update(postDocRef, { ...post });
    return batch.commit()
  }

  getLikeeee(postId, userId) {
    const user = this.angularFireStore.collection(`${"posts"}/${postId}/${"likes"}`, ref =>
      ref.where("userId", "==", userId)).snapshotChanges().pipe(
        map(result => {
          return result.map(doc => {
            return {
              id: doc.payload.doc.id,
              ...doc.payload.doc.data() as Like
            } as Like;
          });
        })
      );
    return user;
  }

  getDisikeeee(postId, userId) {
    const user = this.angularFireStore.collection(`${"posts"}/${postId}/${"dislike"}`, ref =>
      ref.where("userId", "==", userId)).snapshotChanges().pipe(
        map(result => {
          return result.map(doc => {
            return {
              id: doc.payload.doc.id,
              ...doc.payload.doc.data() as Like
            } as Like;
          });
        })
      );
    return user;
  }

  getLikeeee1(post1Id, userId) {
    const user = this.angularFireStore.collection(`${"posts"}/${post1Id}/${"likes"}`, ref =>
      ref.where("userId", "==", userId)).snapshotChanges().pipe(
        map(result => {
          return result.map(doc => {
            return {
              id: doc.payload.doc.id,
              ...doc.payload.doc.data() as Like
            } as Like;
          });
        })
      );
    return user;
  }

  getDisikeeee1(post1Id, userId) {
    const user = this.angularFireStore.collection(`${"posts"}/${post1Id}/${"dislike"}`, ref =>
      ref.where("userId", "==", userId)).snapshotChanges().pipe(
        map(result => {
          return result.map(doc => {
            return {
              id: doc.payload.doc.id,
              ...doc.payload.doc.data() as Like
            } as Like;
          });
        })
      );
    return user;
  }


  async addLikeeee(post1Id: string, userId) {
    console.log("wfkjsnjf")
    const likeDocRef = this.angularFireStore.collection(`${"posts"}/${post1Id}/${"likes"}`).ref.doc();
    const batch = this.angularFireStore.firestore.batch();
    batch.set(likeDocRef, {
      isLiked: true,
      created_on: Date.now(),
      userId: userId,
    } as Like)
    return batch.commit()
  }

  updateLikeeee(post1Id: string, likeId: string) {
    const likeDocRef = this.angularFireStore.doc(`${"posts"}/${post1Id}/${"likes"}/${likeId}`);
    return likeDocRef.delete()
  }

  async addDislikeeee(post1Id: string, userId, likeId: string) {
    console.log("wfkjsnjf")
    const dislikeDocRef = this.angularFireStore.collection(`${"posts"}/${post1Id}/${"dislike"}`).ref.doc();
    const likeDocRef = this.angularFireStore.doc(`${"posts"}/${post1Id}/${"likes"}/${likeId}`).ref;
    const batch = this.angularFireStore.firestore.batch();
    batch.delete(likeDocRef)
    batch.set(dislikeDocRef, {
      isDisliked: true,
      created_on: Date.now(),
      userId: userId
      // isLiked: true,
      // created_on: Date.now(),
      // userId: userId,
    } as Dislike)
    return batch.commit()
  }

  updateDislikeeee(post1Id: string, dislikeId: string) {
    const likeDocRef = this.angularFireStore.doc(`${"posts"}/${post1Id}/${"dislike"}/${dislikeId}`);
    return likeDocRef.delete()
  }



  getLike(postId, userId) {
    const user = this.angularFireStore.collection(`${"posts"}/${postId}/${"likes"}`).snapshotChanges().pipe(
      map(result => {
        return result.map(doc => {
          return {
            id: doc.payload.doc.id,
            ...doc.payload.doc.data() as Like
          } as Like;
        });
      })
    );
    return user;
  }

  getDislike(postId, userId) {
    const user = this.angularFireStore.collection(`${"posts"}/${postId}/${"dislike"}`).snapshotChanges().pipe(
      map(result => {
        return result.map(doc => {
          return {
            id: doc.payload.doc.id,
            ...doc.payload.doc.data() as Like
          } as Like;
        });
      })
    );
    return user;
  }






















  // async addL(userId: string, likeData: Like, post: User) {
  //   const LikeDocRef = this.angularFireStore.collection(`${"users"}/${userId}/${"likes"}`).ref.doc();
  //   const postDocRef = this.angularFireStore.doc(`users/${userId}`).ref;
  //   const batch = this.angularFireStore.firestore.batch();
  //   batch.set(LikeDocRef, { ...likeData });
  //   batch.update(postDocRef, { ...post });
  //   return batch.commit()
  // }


}
