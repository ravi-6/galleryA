import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Like } from 'src/app/models/like.model';
import { Users } from 'src/app/models/users.model';
import { SnackBarService } from './snack-bar.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private angularFirestore: AngularFirestore,
    private snackBarService: SnackBarService
  ) { }

  async addUser(like: Like, users: Users, postKey) {
    const LikeDocRef = this.angularFirestore.collection(`${"user"}/${postKey}/${"likes"}`).ref.doc();
    const user = this.angularFirestore.collection(`posts`).doc(postKey).ref;
    const batch = this.angularFirestore.firestore.batch();
    batch.set(LikeDocRef, { ...like });
    batch.set(user, { ...users });
    return batch.commit().then((_) => {
      this.snackBarService.openSnackBar('Member added successfully', true);
      return true;
    }).catch((e) => {
      this.snackBarService.openSnackBar('An error occurred while adding members', false);
      return false;
    });
  }
}
