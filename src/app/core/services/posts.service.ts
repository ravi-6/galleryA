import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Like } from 'src/app/models/like.model';
import { Post } from 'src/app/models/post.model';
import { SnackBarService } from './snack-bar.service';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  dbposts: string = 'posts'

  constructor(
    private angularFirestore: AngularFirestore,
    private snackBarService: SnackBarService
  ) { }

  getPosts() {
    const user = this.angularFirestore.collection('posts').snapshotChanges().pipe(
      map(result => {
        return result.map(doc => {
          return {
            id: doc.payload.doc.id,
            ...doc.payload.doc.data() as Post
          } as Post;
        });
      })
    );
    return user;
  }

  getLikes(postID: string) {
    const user = this.angularFirestore.collection(`${"posts"}/${postID}/${"likes"}`).snapshotChanges().pipe(
      map(result => {
        return result.map(doc => {
          return {
            id: doc.payload.doc.id,
            ...doc.payload.doc.data() as Like
          } as Like;
        });
      })
    );
    return user;
  }

  // getPostById(postId: string) {
  //   return this.angularFirestore.doc<Post>(`posts/${postId}`).snapshotChanges().pipe(
  //     map(doc => {
  //       return {
  //         id: doc.payload.id,
  //         ...doc.payload.data()
  //       } as Post;
  //     })
  //   );
  // }

  async addLike(postId: string, like: Like, post: Post) {
    const LikeDocRef = this.angularFirestore.collection(`${"posts"}/${postId}/${"likes"}`).ref.doc();
    const postDocRef = this.angularFirestore.doc(`posts/${postId}`).ref;
    const batch = this.angularFirestore.firestore.batch();
    batch.set(LikeDocRef, { ...like });
    batch.update(postDocRef, { ...post });
    return batch.commit().then((_) => {
      this.snackBarService.openSnackBar('Member added successfully', true);
      return true;
    }).catch((e) => {
      this.snackBarService.openSnackBar('An error occurred while adding members', false);
      return false;
    });
  }

  async updateLike(postId: string, uid: string) {
    const userId = localStorage.getItem('userId');
    const batch = this.angularFirestore.firestore.batch();
    const postDocRef = this.angularFirestore.doc(`${this.dbposts}/${postId}`).ref;
    batch.set(postDocRef, {
      Likes: uid
    }, { merge: true });
  }

}
