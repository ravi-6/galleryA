import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/models/user.model';



import { switchMap } from 'rxjs/operators';
// import { auth } from 'firebase/app';
// import { AUTH_ERROR, DB, VERIFICATION_REDIRECT_URL, PASS_RESET_REDIRECT_URL } from 'src/app/configs/app-settings.config';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // user$: Observable<User>;
  // dbUsersClaims: string = 'users_claims';
  // firebaseUser$: Observable<firebase.User>;

  // constructor(
  //   private _afAuth: AngularFireAuth,
  //   private _af: AngularFirestore,
  //   private _router: Router,
  //   private _storesService: StoresService,
  //   private afa: AngularFireAnalytics
  // ) {
  //   this.firebaseUser$ = this._afAuth.user;
  //   this.user$ = this._afAuth.authState.pipe(
  //     switchMap(user => {
  //         if(user) {
  //           localStorage.setItem('userId', user.uid);
  //           localStorage.setItem('email', user.email);
  //           return this._af.doc<User>(`${DB.users}/${user.uid}`).valueChanges();
  //         } else {
  //           return of(null);
  //         }
  //     })
  //   );
  // }

  // async loginWithGoogle() {
  //   try {
  //     const provider = new auth.GoogleAuthProvider();
  //     provider.setCustomParameters({
  //       prompt: 'select_account'
  //     });
  //     return await this._afAuth.signInWithPopup(provider).then(async result => {
  //       if(result.additionalUserInfo.isNewUser) {
  //         await this.createUser(result.user);
  //         this.afa.logEvent('sign_up', {
  //           operation_type: 'google',
  //           operation_status: 'success',
  //           operation_msg: 'Signed up successfully',
  //           userId: result.user.uid,
  //           email: result.user.email
  //         });
  //       } else {
  //         this.afa.logEvent('login', {
  //           operation_type: 'google',
  //           operation_status: 'success',
  //           operation_msg: 'Logged in successfully',
  //           userId: result.user.uid,
  //           email: result.user.email
  //         });
  //       }
  //       await this._storesService.updateInvitation(result.user.email);
  //       return result.user;
  //     });
  //   } catch(e) {
  //     console.log(e);
  //     this.afa.logEvent('login', {
  //       operation_type: 'google',
  //       operation_status: 'error',
  //       operation_msg: e.toString()
  //     });
  //     if(AUTH_ERROR[e.code]) {
  //       alert(AUTH_ERROR[e.code]);
  //     } else {
  //       alert(AUTH_ERROR["auth/default"]);
  //     }
  //     return undefined;
  //   }
  // }

  // async login(email: string, password: string) {
  //   try {
  //     await this._afAuth.signInWithEmailAndPassword(email, password).then(async result => {
  //       await this._storesService.updateInvitation(result.user.email);
  //       this.afa.logEvent('login', {
  //         operation_type: 'email',
  //         operation_status: 'success',
  //         operation_msg: 'Logged in successfully',
  //         userId: result.user.uid,
  //         email: result.user.email
  //       });
  //       this._router.navigate(['profile']);
  //     });
  //   } catch(e) {
  //     this.afa.logEvent('login', {
  //       operation_type: 'email',
  //       operation_status: 'error',
  //       operation_msg: e.toString()
  //     });
  //     if(AUTH_ERROR[e.code]) {
  //       alert(AUTH_ERROR[e.code]);
  //     } else {
  //       alert(AUTH_ERROR["auth/default"]);
  //     }
  //   }
  // }

  // async register(email: string, password: string, name: string) {
  //   try {
  //     var result = await this._afAuth.createUserWithEmailAndPassword(email, password);
  //     await this.createUser(result.user, name);
  //     await this.sendEmailVerification();
  //     await this._storesService.updateInvitation(result.user.email);
  //     this.afa.logEvent('sign_up', {
  //       operation_type: 'email',
  //       operation_status: 'success',
  //       operation_msg: 'Signed up successfully',
  //       userId: result.user.uid,
  //       email: result.user.email
  //     });
  //     this._router.navigate(['login/verify-email']);
  //   } catch(e) {
  //     this.afa.logEvent('sign_up', {
  //       operation_type: 'email',
  //       operation_status: 'error',
  //       operation_msg: e.toString()
  //     });
  //     alert(AUTH_ERROR);
  //   }
  // }

  // async sendEmailVerification() {
  //   (await this._afAuth.currentUser).sendEmailVerification({
  //     url: VERIFICATION_REDIRECT_URL,
  //     handleCodeInApp: true,
  //   });
  // }

  // async sendPasswordResetEmail(passwordResetEmail: string) {
  //   await this._afAuth.sendPasswordResetEmail(passwordResetEmail, {
  //     url: PASS_RESET_REDIRECT_URL,
  //     handleCodeInApp: true,
  //   })
  //   .then((_) => {
  //     this.afa.logEvent('password_reset', {
  //       operation_type: 'email',
  //       operation_status: 'success',
  //       operation_msg: 'Password reset link has been sent to your email.'
  //     });
  //     alert(`Password reset link has been sent to ${passwordResetEmail}. Please check your email inbox for reseting the password.`)
  //   }).catch((e) => {
  //     this.afa.logEvent('password_reset', {
  //       operation_type: 'email',
  //       operation_status: 'error',
  //       operation_msg: e.toString()
  //     });
  //     if(AUTH_ERROR[e.code]) {
  //       alert(AUTH_ERROR[e.code]);
  //     } else {
  //       alert(AUTH_ERROR["auth/default"]);
  //     }
  //   });
  // }

  // async logout() {
  //   await this._afAuth.signOut();
  //   this.afa.logEvent('logout');
  //   localStorage.clear();
  //   this._router.navigate(['login']);
  // }

  // async createUser(firebaseUser: firebase.User, name?: string) {
  //   let formatedEmail = firebaseUser.email.split('@')[0].split('.').join('') + '@' + firebaseUser.email.split('@')[1];
  //   var user: User = {
  //     name: name ? name : firebaseUser.displayName,
  //     email: formatedEmail,
  //     avatar: firebaseUser.photoURL,
  //     created_on: Date.now().valueOf(),
  //     is_deleted: false,
  //     phone_number: firebaseUser.phoneNumber ? firebaseUser.phoneNumber : null,
  //     updated_on: Date.now().valueOf(),
  //   }
  //   this._af.doc<User>(`${DB.users}/${firebaseUser.uid}`).set(user).then(result => {
  //     this.afa.logEvent('users', {
  //       operation_type: 'create',
  //       operation_status: 'success',
  //       operation_msg: 'User created successfully',
  //     });
  //   }).catch(error => {
  //     this.afa.logEvent('users', {
  //       operation_type: 'create',
  //       operation_status: 'error',
  //       operation_msg: error.toString()
  //     });
  //   });
  // }















  userState: any;
  userData: any; // Save logged in user data
  userlog;
  uid: string;

  constructor(
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone


  ) {

    this.afAuth.authState.subscribe(user => {
      this.userData = user;
      if (user) {
        localStorage.setItem('userId', user.uid);
        localStorage.setItem('email', user.email);
      }
    });
  }

  // Returns true when user is looged in and email is verified

  get isLoggedIn(): boolean {

    const user = JSON.parse(this.userState);
    return (user !== null && user.emailVerified !== false) ? true : false;

  }

  SignIn(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });
        this.SetUserData(result.user);
        this.router.navigate(['dashboard']);

      }).catch((error) => {
        window.alert(error.message)
      })

  }

  SignUp(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.SendVerificationMail();
        this.SetUserData(result.user);
        window.alert('User successfully registered. Please login.');
        this.router.navigate(['']);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  SendVerificationMail() {
    return this.afAuth.currentUser.then(u => this.userData.sendEmailVerification())
      .then(() => {
        this.router.navigate(['email-verification']);
      })
  }

  ForgotPassword(passwordResetEmail: string) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
        this.router.navigate(['']);
      }).catch((error) => {
        window.alert(error)
      })
  }




  GoogleAuth() {
    return this.AuthLogin(
      //new auth.GoogleAuthProvider()
      new firebase.default.auth.GoogleAuthProvider()
    );
  }

  AuthLogin(provider: firebase.default.auth.AuthProvider) {
    return this.afAuth.signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        })
        this.SetUserData(result.user);

      }).catch((error) => {
        window.alert(error)
      })
  }

  SetUserData(user: firebase.default.User | null) {

    // let id = this.afs.createId();

    return this.afs.doc(`users/${user.uid}`).set({

      // id: "",
      uid: user?.uid,
      emailId: user?.email,
      name: user?.displayName,
      avatar: user?.photoURL,
      avatarStoragePath: "",
      created_on: Date.now(),
      numberOfPosts: 0,
      gender: "",
      bio: ""

    }, {
      merge: true
    })
  }




  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['']);
    })
  }
}
