import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarComponent } from 'src/app/modules/shared/snack-bar/snack-bar.component';

@Injectable({
  providedIn: 'root'
})
export class SnackBarService {

  constructor(
    private _snackBar: MatSnackBar
  ) { }

  openSnackBar(message: string, isSuccess: boolean) {
    this._snackBar.openFromComponent(SnackBarComponent, {
      data: { message: message, isSuccess: isSuccess },
      duration: 3000
    });
  }
}
