import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/services/auth.guard';
import { DashBoardComponent } from './modules/dash-board/dash-board.component';
import { HomeComponent } from './modules/home/home.component';
import { MyAccountComponent } from './modules/my-account/my-account.component';
import { SignUpComponent } from './modules/sign-up/sign-up.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'signUp', component: SignUpComponent },
  { path: 'dashboard', component: DashBoardComponent, canActivate: [AuthGuard] },
  { path: 'myAccount', component: MyAccountComponent },
  // {
  //   path: '**',
  //   redirectTo: ''
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
