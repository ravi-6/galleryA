// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD5NPh3YDWAepjzUNIL-zIwah0qD0xO9BY",
    authDomain: "gallery-app-8284d.firebaseapp.com",
    databaseURL: "https://gallery-app-8284d-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "gallery-app-8284d",
    storageBucket: "gallery-app-8284d.appspot.com",
    messagingSenderId: "343501229527",
    appId: "1:343501229527:web:5bda626705236f1c704f92",
    measurementId: "G-Z64XTR3YJJ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
